# CMS UKM

Aplikasi CMS UKM 

## Kenapa?

Selama ini UKM kami sudah baik, dari segi organisasi luar, maupun dalam. Akan tetapi semuanya masih berkoordinasi dalam teknologi yang lama, seperti:
- Tidak mempunyai website
- Tidak memiliki e-mail utama
- Storage masih menggunakan third-party
- dll

## Lalu?

Jadi, sambil belajar, saya mencoba untuk membuat suatu CMS untuk UKM. Nantinya CMS ini akan menjawab permasalahan diatas.


## Our Stack
- Apache2 https://httpd.apache.org/
- PHP 7.xx https://www.php.net/releases/7_0_0.php
- Codeigniter 3 https://codeigniter.com/userguide3/index.html
- PostgreSQL https://www.postgresql.org/
- Bootstrap 4 https://getbootstrap.com/docs/4.0/getting-started/introduction/